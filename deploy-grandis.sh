#!/bin/bash

export VUE_APP_BG_COLOR="#DC291E" VUE_APP_SITE_TITLE_URL="grandispriser.no" VUE_APP_SITE_PRODUCT_NAME="Grandiosa" VUE_APP_SITE_TITLE="Grandispriser" VUE_APP_SITE_TYPE="grandis" BASE_URL="https://grandispriser.no" VUE_APP_GA_ID="UA-132355293-3"

rm -rf dist
echo "Copying grandis-public/ to public/"
cp grandis-public/* public/
yarn build
firebase deploy --only hosting:grandis
