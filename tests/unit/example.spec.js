import { shallowMount } from "@vue/test-utils";
import Header from "@/components/Header.vue";

describe("Header.vue", () => {
  it("renders props.msg when passed", () => {
    const wrapper = shallowMount(Header, { slots: { left: "grandis" } });
    expect(wrapper.text()).toMatch("grandis");
  });
});
