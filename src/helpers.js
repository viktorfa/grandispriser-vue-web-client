import { provenanceTypes } from "./enums";

/**
 *
 * @param {Event} event
 * @returns {File|null}
 */
export const getImageFromPasteEvent = (event) => {
  if (!event.clipboardData)
    throw new Error("Invalid paste event, no clipboardData.");
  const { files } = event.clipboardData;
  if (files && files.length > 0 && files[0]) {
    const file = files[0];
    if (file.type.startsWith("image/")) {
      return files[0];
    }
  }
  return null;
};

/**
 *
 * @param {string} str
 * @returns {string|null}
 */
export const extractShopgunOfferId = (str) => {
  const pattern = /(?:[^a-zA-Z0-9]|^)([a-zA-Z0-9]{8})(?:[^a-zA-Z0-9]|$)/;
  const matches = str.match(pattern);
  if (matches && matches.length > 0) {
    return matches[1];
  }
  return null;
};

/**
 *
 * @param {string} str
 * @returns {string|null}
 */
export const extractOfferUri = (str) => {
  const pattern = /([a-z]+:product:[A-Za-z0-9]+)/;
  const matches = str.match(pattern);
  if (matches && matches.length > 0) {
    console.log("matches");
    console.log(matches);
    return matches[1];
  }
  return null;
};

/**
 *
 * @param {string} str
 * @returns {string|null}
 */
export const getOfferUriFromInput = (str) => {
  const offerUri = extractOfferUri(str);
  if (offerUri) {
    return offerUri;
  }
  const shopgunOfferId = extractShopgunOfferId(str);
  if (shopgunOfferId) {
    return `shopgun:product:${shopgunOfferId}`;
  }
  return null;
};

/**
 *
 * @param {string} str
 * @returns {boolean}
 */
export const isImageUrl = (str) => {
  const pattern = /(?:(?:https?:\/\/))[-a-zA-Z0-9@:%._+~#=]{2,256}\.[a-z]{2,4}\b(?:[-a-zA-Z0-9@:%_+.~#?&/=]*(\.jpg|\.png|\.jpeg))/;
  return pattern.test(str);
};

/**
 *
 * @param {Date} date
 * @param {number} [days=0]
 * @returns {Date}
 */
export const addDaysToDate = (date, days = 0) => {
  const result = new Date(date);
  result.setDate(result.getDate() + days);
  return result;
};

/**
 * @param {number|string} price
 * @param {string} [suffix=,-]
 * @returns {string}
 */
export const formatPrice = (price, suffix = ",-") => {
  if (typeof price === "number") {
    return `${price.toFixed(2).replace(".", ",")}${suffix}`;
  } else {
    return price;
  }
};

/**
 *
 * @param {string|object} x
 * @returns {string}
 */
export const getOfferUri = (x) => {
  if (x.id && typeof x.id === "string") x = x.id;
  else {
    throw new Error(`Invalid input ${x}. Need string or offer object,`);
  }
  return `shopgun:product:${x}`;
};

/**
 *
 * @param {string} str
 * @returns {string|null}
 */
export const parsePrice = (str) => {
  return parseFloat(str.replace(",", "."));
};

export const getCustomOffer = (offer) => {
  let price = parsePrice(offer.price);
  let price_text = offer.price;
  if (Number.isFinite(price) && `${price}`.length >= offer.price.length) {
    price_text = "";
  } else {
    price = NaN;
  }
  return {
    ...offer,
    price,
    price_text,
  };
};

/**
 *
 * @param {ProvenanceType} provenanceType
 * @returns {Number}
 */
export const getProvenanceTypeInt = (provenanceType) => {
  switch (provenanceType) {
    case provenanceTypes.CUSTOM:
      return 3;
    case provenanceTypes.SHOPGUN:
      return 2;
    default:
      return 0;
  }
};

/**
 * @implements {Comparator}
 * @param {object} a
 * @param {object} b
 * @returns {Number}
 */
export const offersSortFunction = (a, b) => {
  try {
    const priceResult = a.pricing.price - b.pricing.price;
    return priceResult;
  } catch (error) {
    console.error(error);
    console.error("Error when sorting.");
  }
  return 0;
};
