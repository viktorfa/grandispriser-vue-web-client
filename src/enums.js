export const messageTypes = {
  ERROR: "error",
  SUCCESS: "success",
};

export const provenanceTypes = {
  CUSTOM: "custom",
  KOLONIAL: "kolonial",
  SHOPGUN: "shopgun",
  MENY: "meny",
  EUROPRIS: "europris",
  HANDLERIET: "handleriet",
};
