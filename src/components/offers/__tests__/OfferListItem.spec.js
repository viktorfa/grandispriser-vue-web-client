import { mount } from "@vue/test-utils";
import OfferListItem from "../OfferListItem.vue";
import exampleOffer from "../../../../tests/assets/amp-offer.json";

describe("OfferListItem.vue", () => {
  const rootMock = { $data: { state: {} } };
  let wrapper;

  beforeEach(() => {
    const parent = {
      data() {
        return {
          state: { identityId: "an-identity-id" },
        };
      },
    };
    wrapper = mount(OfferListItem, {
      propsData: { offer: exampleOffer },
      parentComponent: parent,
    });
    wrapper.vm.$root = rootMock;
    wrapper.vm.$forceUpdate();
  });

  it("should render", () => {
    expect(wrapper.find("button").exists()).toBeTruthy();
    expect(wrapper.text()).toMatch(exampleOffer.dealer);
  });
  it("clicking the like button should trigger a method", () => {
    const handleOfferFeedbackStub = jest.fn((type) => {
      console.log("stubbin' handleOfferFeedback " + type);
    });

    const likeButton = wrapper.find("button");
    wrapper.vm.handleOfferFeedback = handleOfferFeedbackStub;
    likeButton.trigger("click");
    expect(handleOfferFeedbackStub.mock.calls.length).toEqual(1);
  });
});
