/* eslint camelcase: 0 */
import { apiUrl, siteType } from "../config/vars";
import { getIdentityId } from "../config/aws";
import { getCustomOffer } from "../helpers";

/**
 *
 * @param {string} offer_uri
 * @returns {Promise<object>}
 */
export const promoteOffer = async (offer_uri) => {
  const { data: identity_id, error } = await getIdentityId();
  if (error) {
    return error;
  }
  const headers = {
    "Content-Type": "application/json",
  };
  const options = {
    method: "PUT",
    body: JSON.stringify({
      offer_uri,
      offer_type: siteType,
      identity_id,
    }),
    headers,
  };
  return getFetchOption(fetch(`${apiUrl}/offers/promoted`, options));
};

/**
 *
 * @param {object} offer
 * @returns {Promise<object>}
 */
export const saveCustomOffer = async (offer) => {
  const { data: identity_id, error } = await getIdentityId();
  if (error) {
    return error;
  }
  const customOffer = getCustomOffer(offer);
  const headers = {
    "Content-Type": "application/json",
  };
  const options = {
    method: "POST",
    headers,
    body: JSON.stringify({ ...customOffer, identity_id, type: siteType }),
  };
  return getFetchOption(fetch(`${apiUrl}/offers/custom`, options), true);
};

/**
 *
 * @param {string} offerUri
 * @returns {Promise<object>}
 */
export const getOffer = async (offerUri) => {
  const path = `/offers/${offerUri}`;
  const options = {
    method: "GET",
  };
  return getFetchOption(fetch(`${apiUrl}${path}`, options), true);
};

/**
 *
 * @returns {Promise<object>}
 */
export const getSpecialOffers = async () => {
  const path = `/offers/special/?type=${siteType}`;
  const options = {
    headers: {
      Accept: "application/json",
    },
    method: "GET",
  };
  return getFetchOption(fetch(`${apiUrl}${path}`, options), true);
};

export const likeOffer = async (offerUri) => {
  const { data: identity_id, error } = await getIdentityId();
  if (error) {
    return error;
  }
  const body = JSON.stringify({
    offer_uri: offerUri,
    identity_id,
  });
  const options = {
    method: "PUT",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body,
  };
  return getFetchOption(fetch(`${apiUrl}/offers/like`, options), true);
};

export const reportOffer = async (offerUri, type) => {
  const { data: identity_id, error } = await getIdentityId();
  if (error) {
    return error;
  }
  const body = JSON.stringify({
    offer_uri: offerUri,
    type,
    identity_id,
  });
  const options = {
    method: "PUT",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body,
  };
  return getFetchOption(fetch(`${apiUrl}/offers/report`, options), true);
};

/**
 *
 * @param {Promise<Response>} _response
 * @param {boolean} [expectJson=false]
 * @returns {Promise<object>}
 */
export const getFetchOption = async (_response, expectJson = false) => {
  try {
    const response = await _response;
    if (response.ok) {
      if (expectJson) {
        return { data: await response.json() };
      }
      return { data: true };
    }
  } catch (error) {
    console.error(error);
    return { error };
  }
};
