const initialState = {
  messages: [],
  offers: [],
  identityId: null,
};
const store = {
  state: initialState,
  loadOffers(offers) {
    this.state.offers = [...offers];
  },
  setIdentityId(id) {
    this.state.identityId = id;
  },
  addMessage(message) {
    this.state.messages = [
      ...this.state.messages,
      { ...message, isRead: false },
    ];
  },
  addOffer(offer) {
    this.state.offers = [offer, ...this.state.offers];
  },
  readMessage(index) {
    const oldMessage = this.state.messages[index];
    this.state.messages = [
      ...this.state.messages.slice(0, index),
      Object.assign({}, oldMessage, { isRead: true }),
      ...this.state.messages.slice(index + 1),
    ];
  },
};

export default store;
