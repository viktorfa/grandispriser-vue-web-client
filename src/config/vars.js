export const apiUrl = process.env.VUE_APP_API_URL;
export const s3BucketRegion = process.env.VUE_APP_S3_BUCKET_REGION;
export const s3BucketName = process.env.VUE_APP_S3_BUCKET_NAME;
export const cognitoIdentityPoolId =
  process.env.VUE_APP_COGNITO_IDENTITY_POOL_ID;
export const siteType = process.env.VUE_APP_SITE_TYPE || "grandis";
export const siteTitle = process.env.VUE_APP_SITE_TITLE || "Grandispriser";
export const siteTitleUrl =
  process.env.VUE_APP_SITE_TITLE_URL || "grandispriser.no";
export const siteProductName =
  process.env.VUE_APP_SITE_PRODUCT_NAME || "Grandiosa";
