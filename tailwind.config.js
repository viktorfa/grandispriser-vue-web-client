const siteType = process.env.VUE_APP_SITE_TYPE || "grandis";

const grandisColors = {
  primary: {
    100: "#FFFCEE",
    200: "#FEF7D4",
    300: "#FEF2B9",
    400: "#FDE985",
    500: "#FCDF51",
    600: "#E3C949",
    700: "#978631",
    800: "#716424",
    900: "#4C4318",
    default: "#FCDF51",
  },
  secondary: {
    100: "#FCEAE9",
    200: "#F6CAC7",
    300: "#F1A9A5",
    400: "#E76962",
    500: "#DC291E",
    600: "#C6251B",
    700: "#841912",
    800: "#63120E",
    900: "#420C09",
    default: "#DC291E",
  },
  "primary-text": {
    100: "#EEE9E6",
    200: "#D5C8C0",
    300: "#BCA69A",
    400: "#8A644E",
    500: "#582102",
    600: "#4F1E02",
    700: "#351401",
    800: "#280F01",
    900: "#1A0A01",
    default: "#582102",
  },
  "secondary-text": {
    100: "#fff",
    200: "#fff",
    300: "#fff",
    400: "#fff",
    500: "#fff",
    600: "#fff",
    700: "#fff",
    800: "#fff",
    900: "#fff",
    default: "#fff",
  },
};
const pepsiColors = {
  primary: {
    100: "#E6EDF5",
    200: "#BFD2E5",
    300: "#99B7D5",
    400: "#4D80B6",
    500: "#004A97",
    600: "#004388",
    700: "#002C5B",
    800: "#002144",
    900: "#00162D",
    default: "#004A97",
  },
  secondary: {
    100: "#E6EDF5",
    200: "#BFD2E5",
    300: "#99B7D5",
    400: "#4D80B6",
    500: "#004A97",
    600: "#004388",
    700: "#002C5B",
    800: "#002144",
    900: "#00162D",
    default: "#004A97",
  },
  "primary-text": {
    100: "#fff",
    200: "#fff",
    300: "#fff",
    400: "#fff",
    500: "#fff",
    600: "#fff",
    700: "#fff",
    800: "#fff",
    900: "#fff",
    default: "#fff",
  },
  "secondary-text": {
    100: "#fff",
    200: "#fff",
    300: "#fff",
    400: "#fff",
    500: "#fff",
    600: "#fff",
    700: "#fff",
    800: "#fff",
    900: "#fff",
    default: "#fff",
  },
};

const colors = { grandis: grandisColors, pepsi: pepsiColors };

module.exports = {
  theme: {
    extend: {
      colors: colors[siteType],
      zIndex: {
        "-10": "-10",
      },
    },
  },
  plugins: [
    function({ addComponents }) {
      const buttons = {
        ".ripple": {
          "background-position": "center",
          transition: "background 0.8s",
        },
        ".ripple:hover": {
          background: `${
            colors[siteType].primary[400]
          } radial-gradient(circle, transparent 1%, ${
            colors[siteType].primary[400]
          } 1%) center/15000%`,
        },
        ".ripple:active": {
          "background-color": `${colors[siteType].primary[300]}`,
          "background-size": "100%",
          transition: "background 0s",
        },
      };

      addComponents(buttons);
    },
  ],
};
