#!/bin/bash

export VUE_APP_BG_COLOR="#004A97" VUE_APP_SITE_TITLE_URL="pepsipriser.no" VUE_APP_SITE_PRODUCT_NAME="Pepsi Max" VUE_APP_SITE_TITLE="Pepsipriser" VUE_APP_SITE_TYPE="pepsi" BASE_URL="https://pepsipriser.no" VUE_APP_GA_ID="UA-132355293-2"

rm -rf dist
echo "Copying pepsi-public/ to public/"
cp pepsi-public/* public/
yarn build
firebase deploy --only hosting:pepsi
